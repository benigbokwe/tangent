<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 15/06/16
 * Time: 13:56
 */

namespace Tangent\Bundle\ApiBundle\Model;

use Doctrine\ORM\EntityRepository;

interface BaseInterface
{
    /**
     * @return EntityRepository
     */
    public function getEntityRepository();
}
