<?php

namespace Tangent\Bundle\ApiBundle\Manager;

use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Tangent\Bundle\ApiBundle\Entity\Product;

class ProductManager extends BaseManager
{
    /**
     * @param Product $entity
     * @param $data
     * @param bool|false $insert
     * @return bool
     */
    public function updateEntity(Product $entity, $data, $insert = false)
    {
        foreach ($data as $name => $value) {
            if ($name != 'id') {
                $setter = 'set' . ucfirst($name);
                if (method_exists($entity, $setter)) {
                    call_user_func_array(array($entity, $setter), array($value));
                }
            }
        }

        if ($insert === true) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Product $entity
     * @param $data
     * @return bool
     */
    public function insertEntity(Product $entity, $data)
    {
        foreach ($data as $item) {
            $this->updateEntity($entity, $item, true);
        }

        return true;
    }

    /**
     * @param Product $entity
     * @return bool
     */
    public function deleteEntity(Product $entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return true;
    }
}
