<?php

namespace Tangent\Bundle\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\Query;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\EntityRepository;

/**
 * Each entity controller must extend this class
 *
 * Class TangentApiController
 * @package Tangent\Bundle\ApiBundle\Controller
 */
abstract  class TangentApiController extends Controller
{

    /**
     * @return EntityRepository
     */
    abstract public function getEntityRepository();

    /**
     * @param $json
     * @return mixed
     * @throws \Exception
     */
    protected function validateJson($json)
    {
        if (false === $json) {
            throw new \Exception('Invalid JSON');
        }

        return json_decode($json);
    }
}
