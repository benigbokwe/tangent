<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 15/06/16
 * Time: 17:28
 */

namespace Tangent\Bundle\ApiBundle\Controller;

use Tangent\Bundle\ApiBundle\Manager\ProductManager;
use Tangent\Bundle\ApiBundle\Repository\ProductsRepository;

trait ProductControllerTrait
{
    /**
     * @return mixed|ProductManager
     */
    public function getProductManager()
    {
        return $this->container->get('tangent_api.product.manager');
    }

    /**
     * @return ProductsRepository
     */
    public function getEntityRepository()
    {
        return $this->getProductManager()->getRepository("TangentApiBundle:Product");
    }
}
