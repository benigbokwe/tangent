<?php
// src/Tangent/Bundle/ApiBundle/Controller/ProductController.php
namespace Tangent\Bundle\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tangent\Bundle\ApiBundle\Entity\Product;

class ProductController extends TangentApiController
{
    use ProductControllerTrait;

    /**
     * @Route("/tangent/product/list", name="tangent_api_list")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listAction()
    {
        $response = $this->getEntityRepository()->findAllProducts();

        return new JsonResponse($response);
    }

    /**
     *  @Route("/tangent/product/list/{id}", name="tangent_api_read")
     * @Method({"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function readAction($id)
    {
        try {
            $response = $this->getEntityRepository()->findProduct($id);
        } catch (\Exception $ex) {
            return new JsonResponse([
                'status'  => 'ERROR',
                'message' => $ex->getMessage()
            ], 500);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/tangent/product/create", name="tangent_api_create")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $content = $request->getContent();
        $entity  = $this->getProductManager()->createNewEntity();

        try {
            $data = $this->validateJson($content);

            $response = [
                'status' => 'OK',
                'message' => 'item(s) created successfully'
            ];

            // check that item has already been added
            if ($this->checkEntityExists($data)) {
                return new JsonResponse($response);
            }

            $this->getProductManager()->insertEntity($entity, $data);
        } catch (\Exception $ex) {
            return new JsonResponse([
                'status'  => 'ERROR',
                'message' => $ex->getMessage()
            ], 500);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/tangent/product/update/{id}", name="tangent_api_update")
     * @Method({"POST"})
     * @param Request $request
     * @param $id
     * @return JsonResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateAction(Request $request, $id)
    {
        try {
            /** @var $entity Product */
            $entity = $this->getEntityRepository()->find($id);

            if (!$entity instanceof Product) {
                throw new NotFoundHttpException('No product found in the database with the given id - ' . $id);
            }

            $data = json_decode($request->getContent(), true);
            $this->getProductManager()->updateEntity($entity, $data);

            $response = [
                'status' => 'OK',
                'message' => 'Record has been updated successfully'
            ];
        } catch (\Exception $ex) {
            return new JsonResponse([
                'status'  => 'ERROR',
                'message' => $ex->getMessage()
            ], 500);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/tangent/product/delete/{id}", name="tangent_api_delete")
     * @Method({"GET"})
     * @param $id
     * @return JsonResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $response = array();

        try {
            $entity = $this->getEntityRepository()->find($id);
            if (!$entity instanceof Product) {
                throw new NotFoundHttpException('No product found in the database with the given id - ' . $id);
            }

            if ($this->getProductManager()->deleteEntity($entity)) {
                $response = [
                    'status'  => 'OK',
                    'message' => 'Record removed successfully'
                ];
            }
        } catch (\Exception $ex) {
            return new JsonResponse([
                'status'  => 'ERROR',
                'message' => $ex->getMessage()
            ], 500);
        }

        return new JsonResponse($response);
    }

    /**
     * checks that item has already been created
     * @param $entityCollection
     * @return bool
     */
    public function checkEntityExists($entityCollection)
    {
        $collections = json_decode(json_encode($entityCollection), true);

        $items = $this->getEntityRepository()->findAllProducts();

        foreach ($collections as $collection) {
            if (is_array($items)) {
                foreach ($items as $item) {
                    unset($item['id']);

                    if ($item == $collection) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
