<?php

namespace Tangent\Bundle\ApiBundle\Repository;

use Doctrine\ORM\Query;
use Tangent\Bundle\ApiBundle\Entity\Product;

/**
 * ProductsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductsRepository extends TangentApiRepository
{
    /**
     * @return array
     */
    public function findAllProducts()
    {
        return
            $this
                ->createQueryBuilder('e')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findProduct($id)
    {
        return
            $this
                ->createQueryBuilder('e')
                ->where('e.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
    }
}
