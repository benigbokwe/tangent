# Tangent REST API Bundle #

Simple rest api bundle to expose CRUD(Create, Read, Update and Delete) operations for each of its entities.

## Installation ##
*  Add to composer.json
```
#!json

 "require": {
            "tangent/rest-api-bundle": "dev-master"
    }
```
*   Register bundle in app/AppKernel.php

```
#!php

$bundles = [
            new Tangent\Bundle\ApiBundle\TangentApiBundle(),
       ];
```

*  To update schema, run the following command in console:

```
#!php

 $ php bin/console doctrine:schema:update --force

```

*  Open  http://127.0.0.1:8000/tangent/product/list - Read all product details

*  Open  http://127.0.0.1:8000/tangent/product/list/{id} - Read one item details

*  Send POST request to create new entity with the json below & Endpoint url:http://127.0.0.1:8000/tangent/product/create

```
#!json
  [
      {
        "name": "Bread",
        "price": 1.99,
        "description": "Super great"
      },
      {
        "name": "Butter",
        "price": 2.99,
        "description": "Discounted item"
      }
    ]
```

*  Send POST request to update existing entity with the json below
Endpoint url: http://127.0.0.1:8000/tangent/product/update/{id}

```
#!json
 {
      "name": "White Bread",
      "price": 2.99,
      "description": "Nice for breakfast"
   }
```
*  Send GET request to delete existing entity with the json below
End point url http://127.0.0.1:8000/tangent/product/delete/{id}

*  To run unit tests type in the console
phpunit -v tests/ApiBundle
