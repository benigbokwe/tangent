<?php

namespace Tests\ApiBundle\Entity;

use Tangent\Bundle\ApiBundle\Entity\Product;
use Tests\ApiBundle\AbstractTestCase;

class ProductTest extends AbstractTestCase
{
    /**
     * @var Product
     */
    private $product;

    protected function setUp()
    {
        parent::setUp();
        $this->product = new Product();
    }

    /**
     * @Group entity
     */
    public function testSettersAndGetters()
    {
        $this->product
            ->setId(1)
            ->setName('test')
            ->setPrice(19.99)
            ->setDescription('test product 123');

       $this->assertEquals($this->product->getId(), 1);
       $this->assertEquals($this->product->getName(), 'test');
       $this->assertEquals($this->product->getPrice(), 19.99);
       $this->assertEquals($this->product->getDescription(), 'test product 123');
    }
}