<?php

namespace tests\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Tangent\Bundle\ApiBundle\Controller\ProductController;
use Tangent\Bundle\ApiBundle\Entity\Product;
use Tangent\Bundle\ApiBundle\Manager\ProductManager;
use Tangent\Bundle\ApiBundle\Repository\ProductsRepository;
use Tests\ApiBundle\AbstractTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductControllerTest extends AbstractTestCase
{
    /**
     * @var ProductController
     */
    private $productController;

    /**
     * @var array|null
     */
    private $resultArray;
    /**
     * @var ProductsRepository
     */
    private $productRepository;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ProductManager
     */
    private $service;

    /**
     * @var Product
     */
    private $product;

    protected function setUp()
    {
        parent::setUp();
        $this->productController = new ProductController();

        $this->resultArray = [
            [
                'id'          => 1,
                'name'        => 'test',
                'price'       => 2.99,
                'description' => 'test new product'
            ] ,
            [
                'id'          => 2,
                'name'        => 'beans',
                'price'       => 3.99,
                'description' => 'new food in the market'
            ]
        ];

        $this->productRepository = $this
            ->getMockBuilder(ProductsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->request   = $this
            ->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->container = $this->getMock(ContainerInterface::class);
        $this->service   = $this
            ->getMockBuilder(ProductManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->productController->setContainer($this->container);

        $this->container
            ->expects($this->any())
            ->method("get")
            ->with('tangent_api.product.manager')
            ->will($this->returnValue($this->service));

        $this->service
            ->expects($this->any())
            ->method("getRepository")
            ->with('TangentApiBundle:Product')
            ->will($this->returnValue($this->productRepository));

        $this->product = new Product();
        $this->product
            ->setId(1)
            ->setName('bread')
            ->setPrice(9.99)
            ->setDescription('test test');
    }

    public function testActionMethodsExists()
    {
        $actions = array(
            'listAction',
            'readAction',
            'createAction',
            'updateAction',
            'deleteAction'
        );

        foreach ($actions as $action) {
            $this->assertTrue(
                method_exists($this->productController, $action),
                'Class does not have method - listAction'
            );
        }
    }

    public function testListAction()
    {
        $this->productRepository->expects($this->once())
            ->method('findAllProducts')
            ->will($this->returnValue($this->resultArray));

        $response = $this->productController->listAction($this->request);
        $responseArray = json_decode($response->getContent(), true);

        $this->assertInstanceOf(get_class(new JsonResponse()), $response);
        $this->assertEquals(count($responseArray), 2);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testReadAction()
    {
        $this->productRepository->expects($this->once())
            ->method('findProduct')
            ->with(1)
            ->will($this->returnValue($this->resultArray[0]));

        $response = $this->productController->readAction(1);
        $responseArray = json_decode($response->getContent(), true);

        $this->assertInstanceOf(get_class(new JsonResponse()), $response);
        $this->assertEquals(count($responseArray), 4);
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testCreateAction()
    {
        $content = json_encode($this->resultArray, true);
        $this->request
            ->expects($this->once())
            ->method("getContent")
            ->will($this->returnValue($content));

        $this->service
            ->expects($this->once())
            ->method("createNewEntity")
            ->will($this->returnValue($this->product));

        $this->productRepository->expects($this->once())
            ->method('findAllProducts')
            ->will($this->returnValue($this->resultArray));

        $this->service
            ->expects($this->once())
            ->method("insertEntity")
            ->will($this->returnValue($this->product));

        $response = $this->productController->createAction($this->request);

        $this->assertContains("item(s) created successfully", $response->getContent());
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testUpdateAction()
    {
        $this->productRepository->expects($this->once())
            ->method('find')
            ->with(1)
            ->will($this->returnValue($this->product));

        $content = json_encode($this->resultArray[0], true);

        $this->request
            ->expects($this->once())
            ->method("getContent")
            ->will($this->returnValue($content));

        $this->service
            ->expects($this->once())
            ->method("updateEntity")
            ->with($this->product, $this->resultArray[0])
            ->will($this->returnValue(true));

        $response = $this->productController->updateAction($this->request, 1);

        $this->assertContains("Record has been updated successfully", $response->getContent());
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
