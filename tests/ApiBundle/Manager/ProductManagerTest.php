<?php

namespace tests\ApiBundle\Manager;

use Doctrine\ORM\EntityManager;
use Tangent\Bundle\ApiBundle\Entity\Product;
use Tangent\Bundle\ApiBundle\Manager\ProductManager;
use Tangent\Bundle\ApiBundle\Repository\ProductsRepository;
use Tests\ApiBundle\AbstractTestCase;

class ProductManagerTest extends AbstractTestCase
{
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var Product
     */
    private $product;
    /**
     * @var EntityManager
     */
    protected $entityManager;

    protected function setUp()
    {
        parent::setUp();
        $this->product = new Product();
        $this->product
            ->setId(1)
            ->setName('bread')
            ->setPrice(9.99)
            ->setDescription('test test');

        $this->entityManager = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->productManager = new ProductManager($this->entityManager, get_class($this->product));
    }

    public function testGetRepository()
    {
        $productRepository = $this
            ->getMockBuilder(ProductsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManager
             ->expects($this->once())
             ->method('getRepository')
             ->will($this->returnValue($productRepository));

        $this->assertSame(
            $productRepository,
            $this->productManager->getRepository($this->entityManager, get_class($this->product))
        );
    }

    public function testUpdateEntity()
    {
        $data = [
            'price' => 15.99
        ];

        $this->entityManager
             ->expects($this->once())
             ->method('flush');

        $this->productManager->updateEntity($this->product, $data);

        $this->assertEquals(
            $this->product->getPrice(), $data['price']
        );
    }

    public function testInsertEntity()
    {
        $stdClass = new \stdClass();
        $stdClass->name        = 'butter';
        $stdClass->price       = 1.99;
        $stdClass->description = 'beverage';

        $stdClass2 = clone $stdClass;
        $stdClass2->name        = 'bread';
        $stdClass2->price       = 2.99;
        $stdClass2->description = 'beverage';

        $data = [$stdClass, $stdClass2];

        $product = new Product();

        foreach ($data as $item) {
            $this->productManager->updateEntity($product, $item, true);

            $this->assertEquals($product->getName(), $item->name);
            $this->assertEquals($product->getPrice(), $item->price);
            $this->assertEquals($product->getDescription(), $item->description);
        }

        $this->assertTrue($this->productManager->insertEntity($product, $data));
    }

    public function testDeleteEntity()
    {
        $this->entityManager
            ->expects($this->once())
            ->method('remove')
            ->with($this->product);

        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->assertTrue($this->productManager->deleteEntity($this->product));
    }

    public function testCreateNewEntity()
    {
        $newEntity = $this->productManager->createNewEntity(get_class($this->product));
        $this->assertInstanceOf(get_class($newEntity), $this->product);
    }
}
