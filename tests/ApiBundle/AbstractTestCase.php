<?php

namespace Tests\ApiBundle;

use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\SchemaTool;

abstract class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    protected $kernel;
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    protected function setUp()
    {
        require_once(__DIR__.'../../app/AppKernel.php');

        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();
        parent::setUp();

        // Store the container and the entity manager in test case properties
        //$this->container = $this->kernel->getContainer();
        // $this->entityManager = $this->container->get('doctrine')->getManager();

        // Build the schema for sqlite
        //$this->generateSchema();
    }

    public function tearDown()
    {
        // Shutdown the kernel.
        $this->kernel->shutdown();

        parent::tearDown();
    }

    /**
     * @throws SchemaException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    protected function generateSchema()
    {
        // Get the metadata of the application to create the schema.
        $metadata = $this->getMetadata();

        if (!empty($metadata)) {
            // Create SchemaTool
            $tool = new SchemaTool($this->entityManager);
            $tool->createSchema($metadata);
        } else {
            throw new SchemaException('No Metadata Classes to process.');
        }
    }

    /**
     * Overwrite this method to get specific metadata.
     *
     * @return Array
     */
    protected function getMetadata()
    {
        return $this->entityManager->getMetadataFactory()->getAllMetadata();
    }
}
